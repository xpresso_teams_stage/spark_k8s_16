""" This is a sample pyspark transformer which adds a vectorized columns
to the dataset """

from pyspark.ml import Transformer

from .common.utils import convert_feature_to_rdd


class CustomerTransformer(Transformer):
    """ It is a sample transformer which adds a a new column with the
    vectorization data"""

    def _transform(self, dataset):
        print("Schema from LIBSVM:")
        dataset.printSchema()
        print("Loaded training data as a DataFrame with " +
              str(dataset.count()) + " records.")

        # Show statistical summary of labels.
        label_summary = dataset.describe("label")
        label_summary.show()

        df = convert_feature_to_rdd(dataset)
        return df

